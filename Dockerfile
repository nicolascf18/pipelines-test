FROM node:alpine

WORKDIR /App_Node/src

COPY App_Node/package*.json .

RUN npm install

COPY . .

EXPOSE 3000

CMD [ "node", "App_Node/app.js" ]
